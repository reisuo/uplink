using UnityEngine;

[RequireComponent(typeof(Collider))]
public class Laser : MonoBehaviour {
    [SerializeField] private float explosionForce;
    [SerializeField] private float explosionRadius;

    private void OnEnable() {
        Rigidbody[] rigidbodies = FindObjectsOfType<Rigidbody>();
        foreach (Rigidbody rb in rigidbodies) {
            float squareDistance = Vector3.SqrMagnitude(transform.position - rb.transform.position);
            rb.AddExplosionForce(explosionForce / squareDistance, transform.position, explosionRadius);
        }
    }

    private void OnTriggerEnter(Collider other) {
        Enemy enemy = other.GetComponent<Enemy>();
        if (enemy != null) {
            enemy.Kill();
        }
    }
}
