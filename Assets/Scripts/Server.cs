using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class Server : MonoBehaviour {
    [SerializeField] private Bounds nearBounds;
    [SerializeField] private Bounds farBounds;
    [Header("Sound")]
    [SerializeField] private float impulseVolume;
    [SerializeField] private float activatedVolume;
    [Space]
    [SerializeField] private AudioClip activateSound;
    [SerializeField] private AudioClip deactivatedAmbiance;
    [SerializeField] private AudioClip activatedHum;

    private AudioSource audioSource;
    private Transform activator;
    private bool isActivated;
    private bool isActivatorNear;

    // TODO: Make sure near bounds cannot escape far bounds.
    public Bounds NearBounds {
        get { return nearBounds; }
        set { nearBounds = value; }
    }

    public Bounds FarBounds {
        get { return farBounds; }
        set { farBounds = value; }
    }

    public void Activate() {
        FindObjectOfType<Director>().ServerActivated();
        isActivated = true;
        isActivatorNear = false;
        PlayActivationSounds();
        Events.OnServerActivate(this);
    }

    private void Start() {
        isActivated = false;
        isActivatorNear = false;
        activator = FindObjectOfType<Player>().transform;

        audioSource = GetComponent<AudioSource>();
        audioSource.loop = true;
        audioSource.clip = deactivatedAmbiance;
        audioSource.playOnAwake = true;
    }

    private void Update() {
        if (isActivated) return;

        bool outsideFarBounds = !farBounds.Contains(activator.position - transform.position);
        if (isActivatorNear && outsideFarBounds) {
            isActivatorNear = false;
            Events.OnServerFar(this);
        }

        bool insideNearBounds = nearBounds.Contains(activator.position - transform.position);
        if (!isActivatorNear && insideNearBounds) {
            isActivatorNear = true;
            Events.OnServerNear(this);
        }
    }

    private void PlayActivationSounds() {
      audioSource.PlayOneShot(activateSound, impulseVolume);
      audioSource.clip = activatedHum;
      audioSource.volume = activatedVolume;
      audioSource.PlayDelayed(activateSound.length - 0.1f);
    }
}
