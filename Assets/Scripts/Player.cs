using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

[RequireComponent(typeof(CharacterController))]
public class Player : MonoBehaviour {
    [Header("General")]
    [SerializeField] private float moveSpeed;
    [SerializeField] private float dashBoost;
    [SerializeField] private float dashDuration;
    [SerializeField] private float dashCooldown;
    [SerializeField] private float maxHealth;
    [SerializeField] private float recoveryTime;
    [SerializeField] private float recoverySpeed;
    [SerializeField] private float shakeAmount;
    [Header("Weapon")]
    [SerializeField] private Vector3 laserOrigin;
    [SerializeField] private Transform laser;
    [SerializeField] private float laserCost;
    [Header("Sound")]
    [SerializeField] private float firingVolume;
    [SerializeField] private float hurtVolume;
    [SerializeField] private float killVolume;
    [SerializeField] private float dashVolume;
    [SerializeField] private AudioClip firing;
    [SerializeField] private AudioClip hurt;
    [SerializeField] private AudioClip kill;
    [SerializeField] private AudioClip dash;
    [SerializeField] private AudioClip dashFail;

    private AudioSource audioSource;
    private CharacterController characterController;
    private Vector3 cursorLocation;
    private float timeSinceHurt;
    private float timeSinceDash;
    new private Camera camera;

    public float Health {
        get;
        private set;
    }

    public float LaserCharge {
        get;
        private set;
    }

    public float MaxHealth {
        get { return maxHealth; }
    }


    private void Start() {
        Health = maxHealth;
        timeSinceHurt = 0.0f;
        LaserCharge = 1.0f;
        isFiring = false;
        couldFire = false;
        audioSource = GetComponent<AudioSource>();
        characterController = GetComponent<CharacterController>();
        camera = FindObjectOfType<Camera>();
        laser.gameObject.SetActive(false);
    }

    private void PlayNewClip(AudioClip clip, float volume, bool loop) {
        audioSource.clip = clip;
        audioSource.volume = volume;
        audioSource.loop = loop;
        audioSource.Play();
    }

    private void PlayKillSound() {
        audioSource.PlayOneShot(kill, killVolume);
    }

    private void UpdateMotion() {
        Vector3 input = (Vector3.forward * Input.GetAxis("Vertical"))
            + (Vector3.right * Input.GetAxis("Horizontal"));
        Vector3 motion = input * moveSpeed * Time.deltaTime;
        Vector3 gravity = Time.deltaTime * -Vector3.up * 9.81f;
        characterController.Move(motion + gravity);

        timeSinceDash += Time.deltaTime;
        if (Input.GetButtonDown("Dash")) {
            if (timeSinceDash >= dashCooldown) {
                timeSinceDash = 0;
                Dash(motion.normalized);
            } else {
                PlayNewClip(dashFail, dashVolume, false);
            }
        }
    }

    private void UpdateLook() {
        Ray ray = camera.ScreenPointToRay(Input.mousePosition);
        Plane plane = new Plane(Vector3.up, transform.position.y);
        if (plane.Raycast(ray, out float enter)) {
            cursorLocation = ray.origin + (ray.direction * enter);
        }
        cursorLocation.y = transform.position.y;
        transform.LookAt(cursorLocation, Vector3.up);
    }

    private Vector3 Dash(Vector3 direction) {
        PlayNewClip(dash, dashVolume, false);
        Vector3 velocity = direction * moveSpeed * dashBoost;
        StartCoroutine(DashCoroutine(velocity));
        return velocity;
    }

    private IEnumerator DashCoroutine(Vector3 velocity) {
        float time = 0.0f;
        while (time < dashDuration) {
            time += Time.deltaTime;
            characterController.SimpleMove(velocity);
            yield return null;
        }
    }

    private void Fire() {
        Vector3 origin = transform.position + (Vector3)(transform.localToWorldMatrix * laserOrigin);
        Vector3 direction = (cursorLocation - origin).normalized;
        Vector3 destination = cursorLocation;

        if (Physics.Raycast(origin, direction, out RaycastHit hit)) {
            destination = hit.point;
        }

        Vector3 midpoint = (origin  + destination) / 2.0f;
        float length = Vector3.Distance(origin, destination);

        Vector3 forwards = Vector3.Cross(direction, transform.right);

        laser.position = midpoint;
        laser.localScale = new Vector3(0.4f, length / 2.0f, 0.4f);
        laser.rotation = Quaternion.LookRotation(forwards, direction);
    }

    private bool isFiring;
    private bool couldFire;

    private void UpdateFire() {
        if (Input.GetButtonDown("Fire1")) {
            isFiring = true;
        } else if (Input.GetButtonUp("Fire1")) {
            isFiring = false;
        }

        if (isFiring) {
            LaserCharge = Mathf.Max(0.0f, LaserCharge -= laserCost * Time.deltaTime);
        } else {
            LaserCharge = Mathf.Min(1.0f, LaserCharge += 2.0f * laserCost * Time.deltaTime);
        }

        bool canFire = isFiring && (LaserCharge > 0.0001f);

        if (canFire && !couldFire) {
            PlayNewClip(firing, firingVolume, true);
            laser.gameObject.SetActive(true);
        }

        if (!canFire && couldFire) {
            audioSource.Stop();
            laser.gameObject.SetActive(false);
        }

        if (canFire) {
            Fire();
        }

        couldFire = canFire;
    }

    private void UpdateHealth() {
        if (timeSinceHurt >= recoveryTime) {
            Health = Mathf.Min(Health + recoverySpeed * Time.deltaTime, maxHealth);
        }
        timeSinceHurt += Time.deltaTime;
    }

    private void Update() {
        UpdateMotion();
        UpdateLook();
        UpdateFire();
        UpdateHealth();
    }

    public void Damage(float amount) {
        audioSource.PlayOneShot(hurt, hurtVolume);
        timeSinceHurt = 0.0f;
        Health -= amount;
        if (Health <= 0.0f) {
            SceneManager.LoadScene("Death");
        }
    }

    private void OnDrawGizmosSelected() {
        Gizmos.color = Color.cyan;
        Gizmos.DrawSphere(transform.position + laserOrigin, 0.1f);
    }
}