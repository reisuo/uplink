using UnityEngine;

public class EnemySpawner : MonoBehaviour {
    [SerializeField] private float spawnCooldown;
    [SerializeField] private float joinDistance;
    [SerializeField] private Enemy enemy;

    private readonly int maxEnemyCount = 128;
    public static int EnemyCount { get; set; }

    private float timeSinceSpawn;
    private Enemy lastSpawned;

    private void Start() {
        timeSinceSpawn = 0.0f;
    }

    private void Update() {
        timeSinceSpawn += Time.deltaTime;

        if (timeSinceSpawn > spawnCooldown) {
            timeSinceSpawn -= spawnCooldown;
            if (EnemyCount < maxEnemyCount) Spawn();
        }
    }

    private void Spawn() {
        Enemy newEnemy = Instantiate(enemy, transform.position, Quaternion.identity);
        if (lastSpawned != null) {
            float squareDistance = Vector3.SqrMagnitude(transform.position - lastSpawned.transform.position);
            if (squareDistance < (joinDistance * joinDistance)) {
                SpringJoint joint = lastSpawned.GetComponent<SpringJoint>();
                joint.connectedBody = newEnemy.GetComponent<Rigidbody>();
            }
        }

        EnemyCount += 1;
        lastSpawned = newEnemy;
    }
}
