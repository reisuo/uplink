using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(Door))]
public class DoorEditor : Editor {
    public override void OnInspectorGUI() {
        base.DrawDefaultInspector();
        if (!Application.isPlaying) return;

        Door door = target as Door;

        EditorGUILayout.LabelField("State", door.State.ToString());

        if ((door.State == DoorState.Open) || (door.State == DoorState.Opening)) {
            bool clicked = GUILayout.Button("Close");
            if (clicked) door.Close();
        }

        if ((door.State == DoorState.Closed) || (door.State == DoorState.Closing)) {
            bool clicked = GUILayout.Button("Open");
            if (clicked) door.Open();
        }
    }
}