using UnityEditor;

[CustomEditor(typeof(LevelLoadTrigger))]
public class LevelLoadTriggerEditor : Editor {
    public override void OnInspectorGUI() {
        LevelLoadTrigger trigger = target as LevelLoadTrigger;

        string[] options = new string[EditorBuildSettings.scenes.Length];
        for (int i = 0; i < options.Length; i++) {
            options[i] = EditorBuildSettings.scenes[i].path;
        }

        int old = trigger.Scene;
        trigger.Scene = EditorGUILayout.Popup(trigger.Scene, options);
        if (trigger.Scene == old) return;

        Undo.RecordObject(trigger, "Set target scene");
        PrefabUtility.RecordPrefabInstancePropertyModifications(trigger);
    }
}