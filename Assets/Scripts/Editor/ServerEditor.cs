using UnityEditor;
using UnityEditor.IMGUI.Controls;
using UnityEngine;

[CustomEditor(typeof(Server))]
public class ServerEditor : Editor {
    private static BoxBoundsHandle nearBounds = new BoxBoundsHandle();
    private static BoxBoundsHandle farBounds = new BoxBoundsHandle();

    private void OnSceneGUI() {
        Server server = target as Server;

        nearBounds.center = server.transform.position + server.NearBounds.center;
        nearBounds.size = server.NearBounds.size;
        nearBounds.SetColor(Color.red);
        nearBounds.DrawHandle();
        server.NearBounds = new Bounds(nearBounds.center - server.transform.position, nearBounds.size);

        farBounds.center = server.transform.position + server.FarBounds.center;
        farBounds.size = server.FarBounds.size;
        farBounds.SetColor(Color.blue);
        farBounds.DrawHandle();
        server.FarBounds = new Bounds(farBounds.center - server.transform.position, farBounds.size);
    }
}