using System.Collections;
using UnityEngine;
using UnityEngine.AI;

enum EnemyState {
    Approach,
    Windup,
    Launch,
    Recover,
}

[RequireComponent(typeof(NavMeshAgent), typeof(Rigidbody), typeof(SpringJoint))]
public class Enemy : MonoBehaviour {
    [Header("Movement")]
    [SerializeField] private float force;
    [SerializeField] private float windupForce;
    [SerializeField] private float launchForce;
    [SerializeField] private float launchDistance;
    [SerializeField] private float targetHeight;
    [SerializeField] private float windupTime;
    [SerializeField] private float recoveryTime;
    [Header("Sound")]
    [SerializeField] private AudioClip rolling;
    [SerializeField] private AudioClip windup;
    [SerializeField] private AudioClip attack;
    [SerializeField, Range(0, 1)] private float rollingVolume;
    [SerializeField, Range(0, 1)] private float windupVolume;
    [SerializeField, Range(0, 1)] private float attackVolume;
    [Header("Miscellaneous")]
    [SerializeField] private EnemyDeath death;

    private AudioSource audioSource;
    private Transform target;
    private SpringJoint joint;
    private LineRenderer line;
    private NavMeshAgent agent;
    private new Rigidbody rigidbody;
    private EnemyState state;
    private bool shouldLaunch;
    private bool shouldRecover;

    private void Start() {
        target = FindObjectOfType<Player>().transform;

        agent = GetComponent<NavMeshAgent>();
        rigidbody = GetComponent<Rigidbody>();
        joint = GetComponent<SpringJoint>();
        line = GetComponent<LineRenderer>();
        audioSource = GetComponent<AudioSource>();

        agent.updatePosition = false;
        state = EnemyState.Approach;
        shouldRecover = true;
        shouldLaunch = true;
        PlayAudioClip(rolling, rollingVolume, true);
    }

    private void Update() {
        if (!agent.pathPending) {
            agent.destination = target.position;
        }

        switch (state) {
        case EnemyState.Approach:
            Approach();
            break;
        case EnemyState.Windup:
            Windup();
            break;
        case EnemyState.Launch:
            Launch();
            break;
        default:
            Recover();
            break;
        }

        Rigidbody body = joint.connectedBody;
        if (body == null) {
            line.enabled = false;
        } else {
            line.enabled = true;
            line.positionCount = 2;
            line.SetPositions(new Vector3[] {
                transform.position,
                body.transform.position
            });
        }
    }

    private void PlayAudioClip(AudioClip clip, float volume, bool loop) {
        audioSource.clip = clip;
        audioSource.volume = volume;
        audioSource.loop = loop;
        audioSource.Play();
    }

    private void Approach() {
        float squareDistance = Vector3.SqrMagnitude(target.position - transform.position);
        bool isInLaunchingDistance = squareDistance < (launchDistance * launchDistance);

        if (isInLaunchingDistance) {
            StartCoroutine(WaitForWindup());
            PlayAudioClip(windup, windupVolume, false);
            state = EnemyState.Windup;
        } else {
            agent.nextPosition = transform.position + (agent.desiredVelocity * Time.deltaTime);
            Vector3 thisForce = agent.desiredVelocity.normalized * Time.deltaTime * force;
            Vector3 position = transform.position + (Vector3.up / 2.0f);
            rigidbody.AddForceAtPosition(thisForce, position);
        }
    }

    private void Windup() {
        rigidbody.AddForce(-rigidbody.velocity * windupForce * Time.deltaTime);
        if (shouldLaunch) {
            PlayAudioClip(attack, attackVolume, false);
            state = EnemyState.Launch;
        } 
    }

    private void Launch() {
        Vector3 destination = target.position + (Vector3.up * targetHeight);
        Vector3 vector = (destination - transform.position).normalized * launchForce;
        rigidbody.AddForce(vector);
        StartCoroutine(WaitForRecovery());
        state = EnemyState.Recover;
    }

    private void Recover() {
        if (shouldRecover) {
            PlayAudioClip(rolling, rollingVolume, true);
            state = EnemyState.Approach;
        }
    }

    private IEnumerator WaitForWindup() {
        shouldLaunch = false;
        yield return new WaitForSeconds(windupTime);
        shouldLaunch = true;
    }

    private IEnumerator WaitForRecovery() {
        shouldRecover = false;
        yield return new WaitForSeconds(recoveryTime);
        shouldRecover = true;
    }

    private void OnCollisionEnter(Collision collision) {
        Player player = collision.gameObject.GetComponent<Player>();
        if (player == null) return;
        player.Damage(collision.relativeVelocity.magnitude);
    }

    private void OnDestroy() {
        EnemySpawner.EnemyCount -= 1;
    }

    public void Kill() {
        Instantiate(death, transform.position, Quaternion.identity);
        Destroy(gameObject);
    }
}