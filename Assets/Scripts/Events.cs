public static class Events {
    public delegate void ServerEvent(Server server);

    public static ServerEvent OnServerNear;
    public static ServerEvent OnServerFar;
    public static ServerEvent OnServerActivate;

    static Events() {
        OnServerNear = new ServerEvent((s) => { });
        OnServerFar = new ServerEvent((s) => { });
        OnServerActivate = new ServerEvent((s) => { });
    }
}