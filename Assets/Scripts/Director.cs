using UnityEngine;
using UnityEngine.SceneManagement;

public class Director : MonoBehaviour {
    [SerializeField] private AudioClip doorOpen;
    [SerializeField] private float doorVolume;
    private AudioSource audioSource;

    private int serverCount;

    private void Start() {
        audioSource = GetComponent<AudioSource>();
        serverCount = 0;
    }

    public void ServerActivated() {
        if (SceneManager.GetActiveScene().name == "Tutorial") {
            audioSource.PlayOneShot(doorOpen, doorVolume);
            FindObjectOfType<Door>(true).Open();
        } else {
            serverCount += 1;
            if (serverCount >= 3) {
                FindObjectOfType<Door>(true).Open();
                audioSource.PlayOneShot(doorOpen, doorVolume);
            }
        }
    }
}
