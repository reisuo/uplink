using UnityEngine;
using UnityEngine.SceneManagement;

[RequireComponent(typeof(Collider))]
public class LevelLoadTrigger : MonoBehaviour {
    [SerializeField] private int scene;

    public int Scene {
        get { return scene; }
        set { scene = value; }
    }

    private void OnTriggerEnter(Collider other) {
        Player player = other.GetComponent<Player>();
        if (player == null) return;
        SceneManager.LoadScene(scene);
    }
}