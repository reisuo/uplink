using System.Collections;
using UnityEngine;

[RequireComponent(typeof(Camera))]
public class CameraController : MonoBehaviour {
    [SerializeField] private Vector3 offset;
    [SerializeField] private float smoothTime;
    [SerializeField, Range(0, 1)] private float screenShakeMultiplier;
    [SerializeField] private float damageScreenShakeTime;
    [SerializeField, Range(0, 1)] private float cursorStrength;

    private float screenShakeAmount;
    private float screenShakeTime;
    private float lastPlayerHealth;

    public void Shake(float amount, float time) {
        screenShakeAmount = amount * screenShakeMultiplier;
        screenShakeTime = time;
    }

    new private Camera camera;
    private Vector3 velocity;
    private Player player;

    private void Start() {
        camera = GetComponent<Camera>();
        velocity = Vector3.zero;
        player = FindObjectOfType<Player>();
        lastPlayerHealth = player.Health;
        screenShakeAmount = 0.0f;
        screenShakeTime = 0.0f;
        StartCoroutine(ScreenShake());
    }

    private Vector3 GetCursorPosition() {
        Ray ray = camera.ScreenPointToRay(Input.mousePosition);
        Plane plane = new Plane(Vector3.up, 0.0f);

        if (plane.Raycast(ray, out float enter)) {
            Vector3 intersection = ray.origin + (ray.direction * enter);
            intersection.y = offset.y;
            return intersection;
        } else {
            return transform.position;
        }
    }

    private void Update() {
        if (Mathf.Abs(lastPlayerHealth - player.Health) > 0.0001f) {
            float change = player.Health - lastPlayerHealth;
            lastPlayerHealth = player.Health;
            if (change >= 0.0f) return;
            Shake(change, damageScreenShakeTime);
        }
    }

    private void LateUpdate() {
        Vector3 cursorPosition = GetCursorPosition();
        Vector3 targetPosition = player.transform.position + offset;
        Vector3 finalPosition = Vector3.Lerp(targetPosition, cursorPosition, cursorStrength);
        Vector3 nextPosition =
            Vector3.SmoothDamp(transform.position, finalPosition, ref velocity, smoothTime);
        transform.position = nextPosition;
    }

    private IEnumerator ScreenShake() {
        while (true) {
            yield return null;

            if (screenShakeTime > 0.0f) {
                screenShakeTime -= Time.deltaTime;
                Vector3 thisOffset = Random.onUnitSphere * screenShakeAmount;
                thisOffset.Scale(new Vector3(1, 0, 1));
                transform.position += thisOffset;
                yield return null;
                transform.position -= thisOffset;
            }
        }
    }
}