using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class UIController : MonoBehaviour {
    [Header("Timings")]
    [SerializeField] private float serverActivationTime;
    [SerializeField] private float interactTimeFalloff;
    [SerializeField] private float joinedTooltipTime;
    [Header("References")]
    [SerializeField] private GameObject interactionTooltip;
    [SerializeField] private GameObject joinedTooltip;
    [SerializeField] private Image interactionImage;
    [SerializeField] private Image healthIndicator;
    [SerializeField] private Image chargeIndicator;

    private Server nearbyServer;
    private Player player;
    private float interactTime;

    private void Start() {
        Events.OnServerNear += OnServerNear;
        Events.OnServerFar += OnServerFar;
        Events.OnServerActivate += OnServerActivate;

        joinedTooltip.SetActive(false);
        interactionTooltip.SetActive(false);
        interactTime = 0.0f;
        nearbyServer = null;
        player = FindObjectOfType<Player>();
    }

    private void OnDestroy() {
        Events.OnServerNear -= OnServerNear;
        Events.OnServerFar -= OnServerFar;
        Events.OnServerActivate -= OnServerActivate;
    }

    private void Update() {
        UpdateUI();
        UpdateInteraction();
    }

    private void UpdateUI() {
        interactionImage.fillAmount = Mathf.Clamp01(interactTime / serverActivationTime);
        chargeIndicator.fillAmount = Mathf.Clamp01(player.LaserCharge);
        healthIndicator.fillAmount = Mathf.Clamp01(player.Health / player.MaxHealth);
    }

    private void UpdateInteraction() {
        if ((nearbyServer == null) || !Input.GetButton("Interact")) {
            interactTime = Mathf.Max(0.0f, interactTime - interactTimeFalloff * Time.deltaTime);
        } else {
            interactTime += Time.deltaTime;
        }

        if (interactTime >= serverActivationTime) {
            nearbyServer.Activate();
        }
    }

    private IEnumerator DisplayJoinMessage() {
        joinedTooltip.SetActive(true);
        yield return new WaitForSeconds(joinedTooltipTime);
        joinedTooltip.SetActive(false);
    }

    private void OnServerNear(Server s) {
        if (nearbyServer != null) {
            Debug.LogWarning("OnServerNear encountered conflicting servers.");
            return;
        }

        nearbyServer = s;
        interactionTooltip.SetActive(true);
    }

    private void OnServerFar(Server s) {
        if (nearbyServer != s) {
            Debug.LogWarning("OnServerFar encountered conflicting servers.");
            return;
        }

        nearbyServer = null;
        interactionTooltip.SetActive(false);
    }

    private void OnServerActivate(Server s) {
        if (nearbyServer != s) {
            Debug.LogWarning("OnServerActivate encountered conflicting servers.");
            return;
        }

        StartCoroutine(DisplayJoinMessage());
        nearbyServer = null;
        interactionTooltip.SetActive(false);
    }
}
