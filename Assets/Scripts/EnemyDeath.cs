using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class EnemyDeath : MonoBehaviour {
    private void Start() {
        Destroy(gameObject, GetComponent<AudioSource>().clip.length);
    }
}