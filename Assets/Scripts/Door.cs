using UnityEngine;
using UnityEngine.AI;

public enum DoorState {
    Closing,
    Closed,
    Opening,
    Open,
}

[RequireComponent(typeof(BoxCollider), typeof(NavMeshObstacle))]
public class Door : MonoBehaviour {
    [SerializeField] private float speed;
    [SerializeField] private Vector3 closePosition;
    [SerializeField] private Vector3 openPosition;

    private float interpolation;
    private Transform child;
    new private BoxCollider collider;
    private NavMeshObstacle obstacle;

    public DoorState State { get; private set; }

    public void Open() {
        if (State != DoorState.Open) {
            State = DoorState.Opening;
        }
    }

    public void Close() {
        if (State != DoorState.Closed) {
            collider.enabled = true;
            obstacle.enabled = true;
            State = DoorState.Closing;
        }
    }

    private void Start() {
        collider = GetComponent<BoxCollider>();
        obstacle = GetComponent<NavMeshObstacle>();

        State = DoorState.Closed;
        interpolation = 0.0f;
        child = transform.GetChild(0);
    }

    private void UpdatePosition() {
        child.localPosition = Vector3.Lerp(closePosition, openPosition, interpolation);
    }

    private void Update() {
        if (State == DoorState.Closing) {
            interpolation = Mathf.Max(0.0f, interpolation - (Time.deltaTime * speed));
            if (interpolation < 0.00001f) {
                State = DoorState.Closed;
            }
            UpdatePosition();
        }

        if (State == DoorState.Opening) {
            interpolation = Mathf.Min(1.0f, interpolation + (Time.deltaTime * speed));
            if (interpolation > 0.99999f) {
                collider.enabled = false;
                obstacle.enabled = false;
                State = DoorState.Open;
            }
            UpdatePosition();
        }
    }
}